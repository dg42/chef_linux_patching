# InSpec test for recipe linux_patching::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

unless os.windows?
  describe crontab(path: '/etc/cron.d/linux_patching_patch') do
    its('commands') { should include '/usr/local/sbin/linux_patching_patch' }
  end

  describe crontab(path: '/etc/cron.d/linux_patching_patch').commands('/usr/local/sbin/linux_patching_patch') do
    its('weekdays') { should cmp '0' }
    its('hours') { should cmp '3' }
    its('minutes') { should cmp '0' }
  end
end
