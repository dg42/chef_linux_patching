# linux_patching CHANGELOG

This file is used to list changes made in each version of the linux_patching cookbook.

# 0.1.1 (2019-09-25)
- Add CONTRIBUTING.md
- Add TESTING.md
- Update README.md

# 0.1.0 (2019-09-25)
- Initial release.
