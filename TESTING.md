# Testing linux_patching

This cookbook utilizes the [Test Kitchen](https://github.com/test-kitchen/test-kitchen) project for testing.  The pipeline uses the EC2 driver, but any test-kitchen driver can be used by creating a `.kitchen.local.yml` file and overriding any settings.
