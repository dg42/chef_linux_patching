#
# Cookbook:: linux_patching
# Spec:: default
#
# Copyright:: 2019, Dan Gordon
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# for a complete list of available platforms and versions see:
# https://github.com/chefspec/fauxhai/blob/master/PLATFORMS.md

require 'spec_helper'

describe 'linux_patching::default' do
  context 'When all attributes are default, on Ubuntu 18.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu', version: '18.04') do |node|
        node.normal['linux_patching']['patch'] = {
          'enable' => true,
          'hour' => 3,
          'minute' => 0,
          'monthly' => nil,
          'platforms' => 'all',
          'reboot' => true,
          'splay' => 0,
          'weekly' => 'sunday',
        }
        node.normal['linux_patching']['pre_config']['enable'] = false
        node.normal['linux_patching']['post_config']['enable'] = false
      end
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end

  context 'When all attributes are default, on CentOS 7' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'centos', version: '7') do |node|
        node.normal['linux_patching']['patch'] = {
          'enable' => true,
          'hour' => 3,
          'minute' => 0,
          'monthly' => nil,
          'platforms' => 'all',
          'reboot' => true,
          'splay' => 0,
          'weekly' => 'sunday',
        }
        node.normal['linux_patching']['pre_config']['enable'] = false
        node.normal['linux_patching']['post_config']['enable'] = false
      end
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
