# Contributing to @dg42 Cookbooks

Please submit a pull request, issue, or enhancement to the project.  I am usually fairly responsive and very open to improvements to the project.
